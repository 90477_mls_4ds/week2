# Week2 - 17-19 November 2021

Day | Lesson 
---|---
17 November | Solution Exercise 3 - Lesson3 (see week1/exercises))
17 November | [DataFrame in Pandas Library](./slides/dataframe_pandas.pdf)
17 November | [Top City in NY application](./slides/top_us_cities_dataframe_pandas_example.pdf)
17 November | Structure code in notebook (see [setup](./scripts/setup.ipynb), [creation of configuration files](./scripts/conf_files.ipynb) and [handle civil protection header information](./scripts/handle_civil_protection_header_information.ipynb) 
18 November | [Solution Exercise 4 - Lesson4](./exercises/Lesson4_exercise1.ipynb) 
18 November | [Solution Exercise 5 - Lesson4](./exercises/Lesson4_exercise2.ipynb)
18 November | [Italian COVID 19 application](./scripts/covid_data_example.ipynb)
18 November | [California housing price application](./scripts/california_housing_price_data_example.ipynb)
19 November | [Italian COVID_19 application](./scripts/covid_data_example.ipynb)
19 November | [matplotlib Library](./slides/matplotlib_library.pdf)
24 November | [Solution Exercise - Lesson5](./exercises/Lesson5_exercise.ipynb)
24 November | [Solution Exercise - Lesson6](./exercises/Lesson6_exercise.ipynb)

NOTE: 
* Notebook [ISTAT_data_en.ipynb](./scripts/ISTAT_data_en.ipynb) modified according to the materials in week2
* Notebook [handle_civil_protection_header_information.ipynb](./scripts/handle_civil_protection_header_information.ipynb) modified to create a new json file
* Renamed region_it_en.json in covid_region_it_en.json
* Renamed province_it_en.json in covid_province_it_en.json
* Notebook [Italian COVID 19 application](./scripts/covid_data_example.ipynb) updated
* Notebook [Solution Exercise - Lesson5](./exercises/Lesson5_exercise.ipynb) updated with plotly
