#!/usr/bin/env python
# coding: utf-8

# ## Setup
# 
# First, let's import a few common modules, ensure MatplotLib plots figures inline, set locations and prepare a function to save the figures. 
# 
# `%matplotLib inline` allows you to show resulting plots below the code cell that produced it and store plots in the notebook document. 

# In[5]:


# Python ≥3.5 is required
import sys
assert sys.version_info >= (3, 5)

# Common imports
import numpy as np
import os
import pandas as pd

# To plot pretty figures
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl 
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

# Where to save the data, results and images
LESSON_ROOT_DIR = "../"

CONF_PATH = os.path.join(LESSON_ROOT_DIR, "conf")
os.makedirs(CONF_PATH, exist_ok=True)

DATA_PATH = os.path.join(LESSON_ROOT_DIR, "data")
os.makedirs(DATA_PATH, exist_ok=True)

RESULTS_PATH = os.path.join(LESSON_ROOT_DIR, "results")
os.makedirs(RESULTS_PATH, exist_ok=True)

IMAGES_PATH = os.path.join(LESSON_ROOT_DIR, "images")
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    
    if tight_layout:
        plt.tight_layout()
        
    plt.savefig(path, format=fig_extension, dpi=resolution)


# In[ ]:




