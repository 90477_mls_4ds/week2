#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json as jn
import setup

# In[5]:


def create_conf_file(d, name):
    filename = f'{setup.CONF_PATH}/%s.json' % name
    with open(filename, 'w') as fd:
        jn.dump(d, fd)


# In[6]:


def load_conf_file(name):
    filename = f'{setup.CONF_PATH}/%s.json' % name
    with open(filename) as fd:
        return jn.load(fd)


# In[ ]:




